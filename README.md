# aleph
Tutorials and scripts for Aleph
***

## Obtaining an account

Fill out the form at https://it.chem.ox.ac.uk/chemlinux.html. If there are any problems email help@itsupport.chem.ox.ac.uk.

## Logging in

To logon to Aleph on mac open a terminal window and use the following command, replacing abcd1234 with your username.

```bash
ssh abcd1234@aleph.chem.ox.ac.uk
```
**Note**: You need to be connected to the [Oxford VPN](https://help.it.ox.ac.uk/network/vpn/index) if you're connecting via Eduroam.


## Using the cluster

To access files of the scratch directory while a job is running:

```bash
ssh comp08XX
cd /scratch/scratch/2XXXXX.1.multiway.q/
```
where comp08XX is the compute node and 2XXXXX is the job ID. Both are listed when  `qstat -u abcd1234` is run.
You may be asked if you wish to add the node to the list of known hosts if this is the first time you have logged in to this node. You should do this.
