#!/usr/bin/env python3
from subprocess import Popen, PIPE

n_total_cores = (10 * 24) + 28
users_and_names = {'wolf5262': 'Tom W',
                   'wolf5195': 'Tanya',
                   'chem1377': 'Alex D',
                   'sann4830': 'Ali (I)',
                   'magd5103': 'Kate',
                   'ball4935': 'Tom Y',
                   'orie3990': 'Matina',
                   'chem1537': 'Said',
                   'chri4547': 'Henry',
                   'quee3757': 'Tristan',
                   'univ4445': 'Niamh',
                   'wadh4597': 'Ally (II)',
                   'chem1540': 'Tomasz',
                   'magd4438': 'Joe F',
                   'linc3717': 'Jonathan',
                   'duarte': 'Fernanda',
                   'quee3457': 'Chuyan',
                   'newc5888': 'Hanwen',
                   'lady6433': 'Chloe',
                   'kebl6219': 'Bernie',
                   'corp2684': 'Janko',
                   'shil5538': 'Nils',
                   'scat7435': 'Jacky',
                   'univ4230': 'Ben',
                   'mert4432': 'Callum',
                   }


class Colors:
    lightblue = "\033[94m"
    lightgreen = "\033[92m"
    lightyellow = "\033[93m"
    lightred = "\033[91m"
    lightmagenta = "\033[95m"
    lightcyan = "\033[96m"
    lightgray = "\033[37m"
    blue = "\033[34m"
    green = "\033[32m"
    yellow = "\033[33m"
    red = "\033[31m"
    magenta = "\033[35m"
    cyan = "\033[36m"
    gray = "\033[90m"
    end = '\033[0m'

    s_colors = [lightblue, lightgreen, lightyellow, lightred, lightmagenta,
                lightcyan, lightgray, blue, green, yellow, red, magenta, cyan,
                gray]


def get_current_users_and_cores():

    users_and_cores = {}
    for line in sp.stdout.readlines():
        if 'comp08' in line.decode('ascii') and not 'comp0812' in line.decode('ascii'):
            n_cores = int(line.decode('ascii').split()[-1])
            user = line.decode('ascii').split()[3]

            if user not in users_and_cores:
                users_and_cores[user] = n_cores

            else:
                users_and_cores[user] += n_cores

    return users_and_cores


if __name__ == '__main__':

    sp = Popen(['qstat'], stdout=PIPE, stderr=PIPE)

    u_and_c = get_current_users_and_cores()
    n_cores_used = sum(u_and_c.values())

    n_person = 0
    print('============================')
    print('Person      n cores      %')
    print('----------------------------')
    for u, c in reversed(sorted(u_and_c.items(), key=lambda item: item[1])):

        if u in users_and_names:

            if n_person < 14:
                name = Colors.s_colors[n_person] + f'{users_and_names[u]:10s}' + Colors.end
            else:
                name = Colors.end + f'{users_and_names[u]:10s}' + Colors.end

            percentage = round(100*c/n_total_cores, 1)
            print(f'{name:<16s}   {c:3d}        {str(percentage):>4}')

        n_person += 1

    print('============================')
    print(f'Total utilisation = {n_cores_used/n_total_cores * 100:.2f} %')
    print('============================')
