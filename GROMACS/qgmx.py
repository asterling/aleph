#!/usr/bin/env python3
import os
import argparse


def get_args():

    parser = argparse.ArgumentParser()
    parser.add_argument("filename", action='store', help='.tpr file to submit to gmx mdrun')
    parser.add_argument("-gpu", action='store_true', help="Run on a GPU + CPU combined. Will use a full node")
    parser.add_argument("-np", type=int, default=4, help="Number of cores to use for the calculation")

    return parser.parse_args()


def print_bash_script(tpr_filename, gpu, np):

    sh_filename = str(tpr_filename.replace('.tpr', '.sh'))

    with open(sh_filename, 'w') as bash_script:
        print('#!/bin/bash \n'
              '#$ -cwd\n'
              '#$ -l s_rt=100:00:00', file=bash_script)

        if gpu:
            print('-gpu was specified. overwriting -np to use all resources')
            print('#$ -pe smp 24', file=bash_script)
            print('#$ -l h=comp0800', file=bash_script)             # comp0800 is the only node with GPUs currently
        else:
            print('#$ -pe smp ' + str(np), file=bash_script)

        print('export ORIG=$PWD\n'
              'export SCR=$TMPDIR', file=bash_script)
        print('cp ' + tpr_filename + ' $SCR\n'
              'cd $SCR\n'
              'export PATH=/usr/local/gromacs-2019.2-GPU/bin/:$PATH', file=bash_script)

        if gpu:
            print('gmx mdrun -gpu_id 01 -deffnm', tpr_filename.replace('.tpr', ''), file=bash_script)
        else:
            print('gmx mdrun -nt', np, '-deffnm', tpr_filename.replace('.tpr', ''), file=bash_script)

        print('cp -R * $ORIG', file=bash_script)
        print('rm *.sh.*', file=bash_script)

    return sh_filename


if __name__ == '__main__':

    args = get_args()
    if not args.filename.endswith('.tpr'):
        exit('Please give a .tpr file. Exiting')

    script_filename = print_bash_script(args.filename, args.gpu, args.np)
    os.system('qsub ' + script_filename)
