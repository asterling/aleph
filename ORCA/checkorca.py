#!/usr/bin/env python3
import os
import subprocess


def completion(files_in_directory):
    output = [file for file in files_in_directory if file.endswith(".out") and not file.startswith("slurm")]

    success = []
    fail = []
    for out in output:
        with open(out, "r") as orca:
            if "****ORCA TERMINATED NORMALLY****" in orca.read():
                success.append(out)
            else:
                fail.append(out)
    return [success, fail]


if __name__ == "__main__":
    files_in_directory = os.listdir()
    all_output = completion(files_in_directory)

    
    print("\n\nTHESE CALCULATIONS RAN SUCCESSFULLY:")
    for elem in all_output[0]:
        print(elem)
    print("\n\nTHESE CALCULATIONS FAILED:")
    for elem in all_output[1]:
        print(elem)
    print("\n\n{} calculations failed" .format(len(all_output[1])))
